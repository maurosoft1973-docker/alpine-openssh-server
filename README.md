# OpenSSH Server Docker image running on Alpine Linux

[![Docker Automated build](https://img.shields.io/docker/automated/maurosoft1973/alpine-openssh-server.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-openssh-server/)
[![Docker Pulls](https://img.shields.io/docker/pulls/maurosoft1973/alpine-openssh-server.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-openssh-server/)
[![Docker Stars](https://img.shields.io/docker/stars/maurosoft1973/alpine-openssh-server.svg?style=for-the-badge&logo=docker)](https://hub.docker.com/r/maurosoft1973/alpine-openssh-server/)

[![Alpine Version](https://img.shields.io/badge/Alpine%20version-v3.17.3-green.svg?style=for-the-badge)](https://alpinelinux.org/)

The Docker images [(maurosoft1973/alpine-openssh-server)](https://hub.docker.com/r/maurosoft1973/alpine-openssh-server/) is based on the minimal [Alpine Linux](https://alpinelinux.org/)  with [OpenSSH Version v3.7.2-r0](http://www.openssh.org/).

##### Alpine Version 3.17.3 (Released Mar 29 2023)
##### OpenSSH Version 3.7.2-r0 (Released 2022-04-28 09:57:16)

## Description

This is image include the ssh server with SFTP Support

## Architectures

* ```:aarch64``` - 64 bit ARM
* ```:armhf```   - 32 bit ARM v6
* ```:armv7```   - 32 bit ARM v7
* ```:ppc64le``` - 64 bit PowerPC
* ```:x86```     - 32 bit Intel/AMD
* ```:x86_64```  - 64 bit Intel/AMD (x86_64/amd64)

## Tags

* ```:latest```         latest branch based (Automatic Architecture Selection)
* ```:aarch64```        latest 64 bit ARM
* ```:armhf```          latest 32 bit ARM v6
* ```:armv7```          latest 32 bit ARM v7
* ```:ppc64le```        latest 64 bit PowerPC
* ```:x86```            latest 32 bit Intel/AMD
* ```:x86_64```         latest 64 bit Intel/AMD
* ```:test```           test branch based (Automatic Architecture Selection)
* ```:test-aarch64```   test 64 bit ARM
* ```:test-armhf```     test 32 bit ARM v6
* ```:test-armv7```     test 32 bit ARM v7
* ```:test-ppc64le```   test 64 bit PowerPC
* ```:test-x86```       test 32 bit Intel/AMD
* ```:test-x86_64```    test 64 bit Intel/AMD
* ```:3.17.3``` 3.17.3 branch based (Automatic Architecture Selection)
* ```:3.17.3-aarch64```   3.17.3 64 bit ARM
* ```:3.17.3-armhf```     3.17.3 32 bit ARM v6
* ```:3.17.3-armv7```     3.17.3 32 bit ARM v7
* ```:3.17.3-ppc64le```   3.17.3 64 bit PowerPC
* ```:3.17.3-x86```       3.17.3 32 bit Intel/AMD
* ```:3.17.3-x86_64```    3.17.3 64 bit Intel/AMD
* ```:3.17.3-3.7.2-r0``` 3.17.3-3.7.2-r0 branch based (Automatic Architecture Selection)
* ```:3.17.3-3.7.2-r0-aarch64```   3.17.3 64 bit ARM
* ```:3.17.3-3.7.2-r0-armhf```     3.17.3 32 bit ARM v6
* ```:3.17.3-3.7.2-r0-armv7```     3.17.3 32 bit ARM v7
* ```:3.17.3-3.7.2-r0-ppc64le```   3.17.3 64 bit PowerPC
* ```:3.17.3-3.7.2-r0-x86```       3.17.3 32 bit Intel/AMD
* ```:3.17.3-3.7.2-r0-x86_64```    3.17.3 64 bit Intel/AMD

## Layers & Sizes

| Version                                                                               | Size                                                                                                                 |
|---------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------|
| ![Version](https://img.shields.io/badge/version-amd64-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-openssh-server/latest?style=for-the-badge)  |
| ![Version](https://img.shields.io/badge/version-armv6-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-openssh-server/armhf?style=for-the-badge)   |
| ![Version](https://img.shields.io/badge/version-armv7-blue.svg?style=for-the-badge)   | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-openssh-server/armv7?style=for-the-badge)   |
| ![Version](https://img.shields.io/badge/version-ppc64le-blue.svg?style=for-the-badge) | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-openssh-server/ppc64le?style=for-the-badge) |
| ![Version](https://img.shields.io/badge/version-x86-blue.svg?style=for-the-badge)     | ![MicroBadger Size (tag)](https://img.shields.io/docker/image-size/maurosoft1973/alpine-openssh-server/x86?style=for-the-badge)     |

## Environment Variables:

### Main OpenSSH parameters:
* `LC_ALL`: default locale (default en_GB.UTF-8)
* `TIMEZONE`: default timezone (default Europe/Brussels)
* `SHELL_TERMINAL`: default shell (bin/sh)
* `PERMITROOTLOGIN`: enable/disable root login (default prohibit-password)
* `SSH_ROOT_PASSWORD`: password for root user (default root)
* `SSH_USER`: user lftp (default demo)
* `SSH_USER_PASSWORD`: password user lftp (default demo)

## Sample Use with gitlab pipeline

### 1. Used as a service, within a job, to send a file
```yalm
sftp_upload:
    image: maurosoft1973/alpine:latest
    services:
        - name: maurosoft1973/alpine-openssh-server:test
          alias: openssh
    script:
        - apk add --no-cache openssh sshpass
        - |
          touch upload.txt
          sshpass -p demo sftp -oStrictHostKeyChecking=no demo@openssh <<EOF
          put upload.txt
          quit
          EOF
```

***
###### Last Update 29.07.2023 10:13:46
