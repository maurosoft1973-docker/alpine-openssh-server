#!/bin/bash
source /scripts/init-alpine.sh

set -e # exit on error
SSH_ROOT_PASSWORD=${SSH_ROOT_PASSWORD:-"root"}
export PERMITROOTLOGIN=${PERMITROOTLOGIN:-"prohibit-password"}
export SSH_USER=${SSH_USER:-"demo"}
SSH_USER_PASSWORD=${SSH_USER_PASSWORD:-"demo"}
DEBUG=${DEBUG:-"0"}

echo -e "Change Root password"
echo -n "root:$SSH_ROOT_PASSWORD" | chpasswd

echo -e "Add group sftp"
addgroup sftp

echo -e "Create User $SSH_USER -> /home/$SSH_USER"
adduser -h /home/$SSH_USER -s /bin/sh -g sftp -D $SSH_USER
echo -n "$SSH_USER:$SSH_USER_PASSWORD" | chpasswd
chown $SSH_USER:$SSH_USER /home/$SSH_USER
chmod 700 /home/$SSH_USER

# Template
export DOLLAR='$'
envsubst < /root/config/ssh_config > /etc/ssh/ssh_config
envsubst < /root/config/sshd_config > /etc/ssh/sshd_config

if [ "$DEBUG" == 1 ]; then
    echo "Display content of /etc/ssh/ssh_config"
    cat /etc/ssh/ssh_config

    echo "Display content of /etc/ssh/sshd_config"
    cat /etc/ssh/sshd_config
fi

ssh-keygen -A
exec /usr/sbin/sshd -D -e "$@"