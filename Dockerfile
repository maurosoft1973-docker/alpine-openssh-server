ARG DOCKER_ALPINE_VERSION

FROM maurosoft1973/alpine:$DOCKER_ALPINE_VERSION

ARG BUILD_DATE
ARG ALPINE_ARCHITECTURE
ARG ALPINE_RELEASE
ARG ALPINE_VERSION
ARG ALPINE_VERSION_DATE
ARG OPENSSH_VERSION
ARG OPENSSH_VERSION_DATE

LABEL \
    maintainer="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    architecture="$ALPINE_ARCHITECTURE" \
    openssh-server-version="$OPENSSH_SERVER_VERSION" \
    alpine-version="$ALPINE_VERSION" \
    build="$BUILD_DATE" \
    org.opencontainers.image.title="alpine-openssh-server" \
    org.opencontainers.image.description="OpenSSH Server $OPENSSH_SERVER_VERSION Docker image running on Alpine Linux" \
    org.opencontainers.image.authors="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    org.opencontainers.image.vendor="Mauro Cardillo" \
    org.opencontainers.image.version="v$OPENSSH_SERVER_VERSION" \
    org.opencontainers.image.url="https://hub.docker.com/r/maurosoft1973/alpine-openssh-server" \
    org.opencontainers.image.source="https://gitlab.com/maurosoft1973-docker/alpine-openssh-server" \
    org.opencontainers.image.created=$BUILD_DATE

RUN \
    echo "" > /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$ALPINE_RELEASE/main" >> /etc/apk/repositories && \
    echo "https://dl-cdn.alpinelinux.org/alpine/v$ALPINE_RELEASE/community" >> /etc/apk/repositories && \
    apk update && \
    apk add --update --no-cache openssh openssh-sftp-server gettext && \
    cp /usr/bin/envsubst /usr/local/bin/

COPY config/ /root/config
COPY files/ /scripts
RUN chmod +x /scripts/run-alpine-openssh-server.sh

EXPOSE 22

ENTRYPOINT ["/scripts/run-alpine-openssh-server.sh"]
